# Application

Universal Helm chart developed by Applifting Cloud Engineering

## Prerequisites

- Kubernetes 1.19+
- Helm 3.2.0+

## Installation using Helm

Helm is a tool for managing Kubernetes charts for installation 
please refer to 
[Helm install guide](https://github.com/helm/helm#install)

To create a Helm release based on this chart:
```bash
helm repo add --username <username> --password <access_token> ace-application https://gitlab.com/api/v4/projects/49150503/packages/helm/stable
helm install my-release ace-application/chart
```

To include this chart in your `Chart.yaml`:
```yaml
dependencies:
  - name: application
    version: x.y.z # check the versions here: https://gitlab.com/applifting-cloud-engineering/helm/application/-/releases
    repository: https://gitlab.com/api/v4/projects/49150503/packages/helm/stable
```

## Contribute

- Use [Conventional commits](https://www.conventionalcommits.org/) to signal to CI that you want to bump the version. The commits will be included in release notes.
- When developing a new feature, open a `feat/` branch. No one is allowed to push to `main` branch.
- Always explain parameters in `values.yaml` so that CI can generate a proper README.

Thank you for your contribution 💙.